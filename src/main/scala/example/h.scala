package example

import example.{EmptyList, ListA}

object Hello extends App {


    def createDoubleItemList(a:Int):ListA[Int] =     {
      var l:ListA[Int] = EmptyList()
      l=l.add(a)
      l.add(a)
    }
    def mulAndPrint(a:Int):Unit = {
      print(s"${a*2},")
    }
    def mul(a:Int):Int = {
      a*2
    }

    var a:ListA[Int]=EmptyList()
    a=a.prepend(1)
    a=a.prepend(3)
    a=a.prepend(4)

    //add
    a=a.add(5)
    println(s"add: ${a}")
    //prepend
    a=a.prepend(7)
    println(s"prepend: ${a}")
    //head
    println(s"head: ${a.head()}")
    //tail
    println(s"tail: ${a.tail()}")
    //IsEmpty
    println(s"IsEmpty: ${a.isEmpty()}")
    //NonEmpty
    println(s"NonEmpty: ${a.nonEmpty()}")
    //Filter
    println(s"Filter: ${a.filter( x=> x>6)}")
    //at
    println(s"at(2): ${a.at(2)}")
    //apply
    println(s"apply(2): ${a.apply(2)}")
    //size
    println(s"size: ${a.size()}")
    //take
    println(s"take(3): ${a.take(3)}")
    //drop
    println(s"drop(2): ${a.drop(2)}")
    //remove
    println(s"remove(2): ${a.remove(2)}")

    //equal
    var b:ListA[Int] = EmptyList()
    b=b.prepend(1)
    b=b.prepend(3)
    b=b.prepend(4)
    println(s"equal: ${a.equal(b)}")
    b=b.add(5)
    b=b.prepend(7)
    println(s"equal: ${a.equal(b)}")
    //head
    println(s"head: ${a.head()}")
    //map
    println(s"Map (multiply items): ${a.map(mul)}")
    //takeWhile
    println(s"takeWhile(x>1): ${a.takeWhile((x:Int)=>x>1)}")
    //foldLeft
    println(s"foldLeft(sum): ${a.foldLeft(0)((a:Int,b:Int)=>a+b)}")
    //concat
    println(s"concat(same list): ${a.concat(a)}")
    //flatMap
    println(s"flatMap(double each item): ${a.flatMap(createDoubleItemList)}")
    //foreach
    print(s"foreach(Mul and print):")
    a.foreach(mulAndPrint)

  }
