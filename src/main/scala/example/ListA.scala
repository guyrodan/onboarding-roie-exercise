package example
import javafx.util.Pair

import scala.collection.immutable.{List, Nil}
import scala.annotation.tailrec




trait ListA[+A] {


  def head():A = {
    this match {
    case RegItem(item,_) => item
    case _ => throw new Exception("The List is empty")
    }
  }

  def tail():ListA[A] = {
      this match {
        case RegItem(item,next) => next
        case _ => throw new Exception("The List is empty")
      }
    }

  def isEmpty():Boolean = {
      this match {
        case RegItem(item,_) => false
        case _ => true
      }
    }

  def nonEmpty():Boolean = {
      !isEmpty()
    }

  def prepend[B >: A](x:B):ListA[B] = {
      RegItem[B](x,this)
    }

  def add[B >: A](x:B):ListA[B] = {
    this match{
      case EmptyList() => RegItem(x,EmptyList())
      case RegItem(value,next) => RegItem(value,next.add(x))
    }

  }

  def filter(pred:A=>Boolean):ListA[A] = {
        this match {
          case EmptyList() => EmptyList()
          case RegItem(x,next) => if(pred(x))  next.filter(pred).prepend(x) else next.filter(pred)

      }
  }

  def at(index:Int): A = {
    this match
      {
      case EmptyList()=> throw new IndexOutOfBoundsException()
      case RegItem(value,next) => if(index==0) return value else next.at(index-1)
      }
  }

  def apply(index:Int): ListA[A] = {
    this match
    {
      case EmptyList()=> throw new IndexOutOfBoundsException()
      case RegItem(value,next) => if(index==0) this else next.apply(index-1)
    }}

  def size() : Int = {

    def sizeCounter(item: ListA[A],counter: Int): Int = {
      item match {
        case EmptyList() => counter
        case RegItem(_, next) => sizeCounter(next,counter+1)
      }
    }

    sizeCounter(this,0)
  }

 override def toString() : String = {

  def toStringInner(item:ListA[A],result: String):String = {
    item match
  {
    case EmptyList() => result
    case RegItem(value,next) => toStringInner(next,s"$result$value,")
  }

  }

  toStringInner(this,"").dropRight(1)

}

  def take(n:Int) : ListA[A] = {
      def takeInner(item:ListA[A],n:Int,res:ListA[A]):ListA[A] =
      {
      item match{
        case EmptyList() => res
        case RegItem(value,next) => if(n>0) takeInner(next,n-1,res.add(value)) else res
      }
      }
      takeInner(this,n,EmptyList())
    }

  def drop(n:Int) : ListA[A] = {

    def dropInner(item:ListA[A],n:Int,result:ListA[A]) : ListA[A] =
      {
        item match {
          case EmptyList() => result
          case RegItem(value,next) => if (n<=0) dropInner(next,n-1,result.add(value)) else dropInner(next,n-1,result)
         }

      }
    dropInner(this,n,EmptyList())
  }

  def remove(n:Int) : ListA[A] = {
      this match {
        case EmptyList()=> this
        case RegItem(value,next) => if(n==0) next.remove(n-1) else RegItem(value,next.remove(n-1))
      }
    }

  def equal[B>:A](toCompare: ListA[B]) : Boolean = {
    (this,toCompare) match {
      case (RegItem(a_value,a_next),RegItem(b_value,b_next)) => if(a_value == b_value) a_next.equal(b_next) else false
      case (EmptyList(),EmptyList()) => true
      case _ => false
    }
  }

  def headOption() : Option[A] = {
  this match{
  case EmptyList() => None
  case RegItem(value,next) => Some(value)
  }
  }

  def map[B](f:A=>B) : ListA[B] = {
      this match{
        case EmptyList() => EmptyList()
        case RegItem(value,next) => RegItem(f(value),next.map(f))
      }
    }

  def takeWhile(pred:A=>Boolean):ListA[A] =  {
      def takeWhileInner(item:ListA[A]) : ListA[A] =
        item match
        {
          case EmptyList() => EmptyList()
          case RegItem(value,next) => if(pred(value)) RegItem(value,takeWhileInner(next)) else EmptyList()
        }

      takeWhileInner(this)
    }

  def foldLeft[B](z:B)(op:(A,B)=>B):B= {
      def foldLeftInner(item:ListA[A],z:B):B =
        {
          item match{
              case EmptyList()=>z
                case RegItem(value,next)=> foldLeftInner(next,op(value,z))
              }
        }
      foldLeftInner(this,z)
    }

  def concat[B>:A](other:ListA[B]):ListA[B] = {
    this match {
      case EmptyList()=> other
      case RegItem(value,next) => RegItem(value,next.concat(other))
    }
  }

  def flatMap[B>:A](f:A=>ListA[B]):ListA[B] = {
      this match{
        case EmptyList() => EmptyList()
        case RegItem(value,next) => f(value).concat(next.flatMap(f))
      }
    }

  def foreach[B](f:A=>B): Unit = {
    this match {
      case RegItem(value,next) => f(value)
         next.foreach(f)
      case EmptyList() =>
    }
  }

}

final case class RegItem[A](var item:A,var next:ListA[A]) extends ListA[A]{

}


case class EmptyList() extends ListA[Nothing] {
}